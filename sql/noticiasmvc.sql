-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-02-2018 a las 19:09:40
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `noticiasmvc`
--
create database `noticiasmvc`;
use `noticiasmvc`;  
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `idNot` int(11) NOT NULL,
  `tituloNot` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `textoNot` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fechaNot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `productos`(

`idPro` int(11) not null,
`nombrePro` varchar(25) COLLATE utf8_spanish_ci not null,
`precioPro` float(7,2) not null,
`unidadesPro` int(11) not null,
`fechaPro` timestamp not null) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
--
-- Volcado de datos para la tabla `noticias`
--
INSERT INTO `productos` values
(1,'El libro de las Sombras',50.75,6,1517529600),
(2,'El Origen',45.25,6,1517529600);


INSERT INTO `noticias` (`idNot`, `tituloNot`, `textoNot`, `fechaNot`) VALUES
(1, 'Las gafas de HoloLens, lo estan petando', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime necessitatibus esse nesciunt harum, totam quas, ex omnis porro earum! Autem numquam mollitia architecto iure perspiciatis quos, maxime eaque aliquam commodi.\r\n<br>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime necessitatibus esse nesciunt harum, totam quas, ex omnis porro earum! Autem numquam mollitia architecto iure perspiciatis quos, maxime eaque aliquam commodi.', 0),
(2, 'Hoy estamos a miercoles', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime necessitatibus esse nesciunt harum, totam quas, ex omnis porro earum! Autem numquam mollitia architecto iure perspiciatis quos, maxime eaque aliquam commodi.\r\n<br>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime necessitatibus esse nesciunt harum, totam quas, ex omnis porro earum! Autem numquam mollitia architecto iure perspiciatis quos, maxime eaque aliquam commodi.', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas`
--

CREATE TABLE `peliculas` (
  `idPel` int(11) NOT NULL,
  `tituloPel` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `textoPel` longtext COLLATE utf8_spanish_ci NOT NULL,
  `imagenPel` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `peliculas`
--

INSERT INTO `peliculas` (`idPel`, `tituloPel`, `textoPel`, `imagenPel`) VALUES
(1, 'El padrino', 'Sipnosis de la pelicula del el Padrino...', 'elpadrino.jpg'),
(2, 'Y si no nos enfadamos', 'PEdzao de pelicula de bud spencer y terence hill', 'enfadamos.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`idNot`);

ALTER TABLE `productos`
  ADD PRIMARY KEY (`idPro`);
--
-- Indices de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD PRIMARY KEY (`idPel`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `idNot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `productos`
  MODIFY `idPro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;  

--
-- AUTO_INCREMENT de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  MODIFY `idPel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
