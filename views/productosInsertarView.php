<h2>Listado de productos - <?php echo $titulo; ?></h2>
<hr>

<form action="index.php?contr=<?php echo $contr; ?>&accion=<?php echo $formAccion; ?>" method="post" role="form">
	<div class="form-group">
		<label for="nombre">Nombre del producto:</label>
		<input type="text" class="form-control" name="formNombre" value="<?php echo $formNombre; ?>">
	</div>
	<div class="form-group">
		<label for="precio">precio del producto:</label>
		<input type="text" class="form-control" name="formPrecio" value="<?php echo $formPrecio; ?>">
	</div>
	<div class="form-group">
		<label for="unidades">unidades del producto:</label>
		<input type="text" class="form-control" name="formUnidades" value="<?php echo $formUnidades; ?>">
	</div>
	<div class="form-group">
		<label for="fecha">Fecha del producto:</label>
		<input type="date" class="form-control" value="<?php echo $formFecha; ?>" name="formFecha">
	</div>
	<div class="form-group">
		<input type="hidden" name="formId" value="<?php echo $formId; ?>">
		<input type="submit" name="enviar" value="enviar">
	</div>
</form>