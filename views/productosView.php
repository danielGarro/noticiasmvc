<h2>Listado de productos</h2>

<hr>
<a href="index.php?contr=<?php echo $contr; ?>&accion=insertar">Alta de producto</a>
<hr>

<?php foreach ($misproductos as $producto) { ?>

<article>	
	<header>
		<h2>
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $producto->id;?>&accion=ver">
				<?php echo $producto->nombre; ?>
			</a>
			-
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $producto->id;?>&accion=borrar">Borrar</a>
			-
			<a href="index.php?contr=<?php echo $contr; ?>&id=<?php echo $producto->id;?>&accion=modificar">Modificar</a>
		</h2>
	</header>
	<section><?php echo $producto->precio; ?></section>
	<section><?php echo $producto->unidades; ?></section>
	<footer><?php echo timestampToFecha($producto->fecha); ?></footer>
</article>

<hr>

<?php } ?>