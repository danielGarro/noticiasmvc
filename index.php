<?php  
//Require de la configuracion de la web
require('includes/config.php');

//Require de funciones utiles
require('includes/funciones.php');

//Require de las classes GENERALES de la web
require('includes/classes/conexion.class.php');

//Como estoy en el contrador PRINCIPAL
//Desde aqui, voy a recoger el controlador que cargare
if(isset($_GET['contr'])){
  $contr=$_GET['contr'];
}else{
  $contr='noticiasController.php';
}

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Noticias - MVC</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>

  </head>
  <body>
    <section class="container">

      <header>
        <h1>Noticias - MVC</h1>
        <hr>
      </header>

      <?php cargarModulo('menu'); ?>
      <hr>

      <section id="principal" class="row">
        <div class="col-md-8">
          <?php require('controllers/'.$contr); ?>
        </div>
        <div class="col-md-4">
          <?php cargarModulo('banner'); ?>
        </div>
      </section>

      <footer>
        <hr>
        <h4>
          Gracias por su visita
          -
          <small>
            <?php cargarModulo('copyright'); ?>
          </small>
          </h4>
      </footer>

    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>