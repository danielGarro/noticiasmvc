<?php  
//Fichero menuController.php

//Llamamos a posibles variables globales
global $contr;

//llamar al modelo
require('modulos/menu/models/elementoMenuModel.php');
require('modulos/menu/models/menuModel.php');

//Configurar ciertos datos
$mimenu=new Menu();
$mimenu->add('Noticias', 'noticiasController.php');
$mimenu->add('Peliculas', 'peliculasController.php');
$mimenu->add('Productos', 'productosController.php');

//Esta es la varible que va a la vista
$elementosMenu=$mimenu->traeElementos($contr);

//Llamar a la vista
require('modulos/menu/views/menuView.php');

?>