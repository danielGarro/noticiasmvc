<?php 
//Llamara a los modelos
require('models/productoModel.php');
require('models/productosModel.php');

//Procesara las ordenes del usuario
$elementos=new Productos();

if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Creo un switch, para seleccionar la accion
switch($accion){
	case 'listado':
		$misproductos=$elementos->listado();
		$vista='productosView.php';
		break;

	case 'ver':
		$misproductos=$elementos->detalle($_GET['id']);
		$vista='productosView.php';
		break;

	case 'insertar':

		$vista='productosInsertarView.php';
		$titulo='Insertar producto';

		$formId='';
		$formNombre='';
		$formPrecio='';
		$formUnidades='';
		$formFecha=date('Y-m-d'); //YYYY-MM-DD;
		$formAccion='insercion';
 
		break;

	case 'insercion':
		//Registro es un ARRAY asociativo, con los mismos nombres
		//que la tabla y el ORDEN !!!!!!!!!!!!!!!
		
		$registro['idPro']=null;
		$registro['nombrePro']=$_POST['formNombre'];
		$registro['precioPro']=$_POST['formPrecio'];
		$registro['unidadesPro']=$_POST['formUnidades'];
		$registro['fechaPro']=fechaToTimestamp($_POST['formFecha']);
		$vista='productosInsertarView.php';
		//llamo al metodo para insertar el registro
		if($elementos->insertar($registro)){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

	case 'borrar':
		if($elementos->borrar($_GET['id'])){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

	case 'modificar':
		$elproducto=$elementos->detalle($_GET['id'])[0];
		$vista='productosInsertarView.php';
		$titulo='Modificar producto';


		$formId=$elproducto->id;
		$formNombre=$elproducto->nombre;
		$formPrecio=$elproducto->precio;
		$formUnidades=$elproducto->unidades;
		$formFecha=date('Y-m-d',$elproducto->fecha);
		$formAccion='modificacion';

		break;

	case 'modificacion':
		
		$registro['nombrePro']=$_POST['formNombre'];
		$registro['precioPro']=$_POST['formPrecio'];
		$registro['unidadesPro']=$_POST['formUnidades'];
		$registro['fechaPro']=fechaToTimestamp($_POST['formFecha']);

		if($elementos->modificar($registro)){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

}

//Pintara las vistas
require('views/'.$vista);

?>