<?php  
// fichero controllers/noticiasController.php

//Llamara a los modelos
require('models/peliculaModel.php');
require('models/peliculasModel.php');

//Procesara las ordenes del usuario
$elementos=new Peliculas();

//Dependiendo de la accion que reciba el controlador
//hago una cosa u otra
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Creo un switch, para seleccionar la accion
switch($accion){
	case 'listado':
		$mispeliculas=$elementos->listado();
		$vista='peliculasView.php';
		break;

	case 'ver':
		$mispeliculas=$elementos->detalle($_GET['id']);
		$vista='peliculasView.php';
		break;

	case 'insertar':

		$vista='peliculasInsertarView.php';
		$titulo='Insertar Pelicula';

		$formId='';
		$formTitulo='';
		$formTexto='';
		$formImagen='';
		$formAccion='insercion';

		break;

	case 'insercion':
		//Registro es un ARRAY asociativo, con los mismos nombres
		//que la tabla y el ORDEN !!!!!!!!!!!!!!!
		$registro['tituloPel']=$_POST['titulo'];
		$registro['textoPel']=$_POST['texto'];
		$registro['imagenPel']=$_FILES['imagen']['name'];
		move_uploaded_file($_FILES['imagen']['tmp_name'], 'imagenes/'.$_FILES['imagen']['name']);
		//llamo al metodo para insertar el registro
		if($elementos->insertar($registro)){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

	case 'borrar':
		if($elementos->borrar($_GET['id'])){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

	case 'modificar':
		$lapelicula=$elementos->detalle($_GET['id'])[0];
		$vista='peliculasInsertarView.php';
		$titulo='Modificar Noticia';

		$formId=$lapelicula->id;
		$formTitulo=$lapelicula->titulo;
		$formTexto=$lapelicula->texto;
		$formImagen=$lapelicula->imagen;
		$formAccion='modificacion';

		break;

	case 'modificacion':
		$registro['idPel']=$_POST['id'];
		$registro['tituloPel']=$_POST['titulo'];
		$registro['textoPel']=$_POST['texto'];
		$registro['imagenPel']=$_FILES['imagen']['name'];
		move_uploaded_file($_FILES['imagen']['tmp_name'], 'imagenes/'.$_FILES['imagen']['name']);

		if($elementos->modificar($registro)){
			header('Location:index.php?contr='.$contr);
		}else{
			//Lo suyo seria currarme una pagina (vista) de error
		}
		break;

}

//Pintara las vistas
require('views/'.$vista);

?>