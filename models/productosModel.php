<?php  

require('models/repositorioModel.php');
class Productos extends Repositorio{

	//conexion, datos, tabla, id ()

	public function __construct(){
		parent::__construct(); //constructor del padre
		$this->tabla='productos';
		$this->id='idPro';
		$this->model='Producto'; //nombre de la clase
	}

}
?>