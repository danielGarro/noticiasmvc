<?php  

//fichero models/repositorioModel.php

class Repositorio{

	public $conexion;
	public $datos;
	public $tabla;
	public $id;
	public $model;

	public function __construct(){
		$this->conexion=Conexion::conectar();
		$this->datos=[];
	}

	public function listado(){
		$sql="SELECT * FROM ".$this->tabla;
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){

			//$dato = new Noticia($registro);
			$dato = new $this->model($registro);
			$this->datos[]=$dato;

		}
		return $this->datos;
	}

	public function detalle($id){
		$sql="SELECT * FROM ".$this->tabla." WHERE ".$this->id."=$id";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){

			//$dato = new Noticia($registro);
			$dato = new $this->model($registro);
			$this->datos[]=$dato;

		}
		return $this->datos;
	}

	public function borrar($id){
		$sql="DELETE FROM ".$this->tabla." WHERE ".$this->id."=$id";
		$consulta=$this->conexion->query($sql);
		if($consulta){
			return true;
		}else{
			return false;
		}
	}

	public function insertar($registro){

		$sql="INSERT INTO ".$this->tabla." VALUES(null";

		foreach ($registro as $value) {
				$sql.=",'".$value."'";
		}

		$sql.=")";

		$consulta=$this->conexion->query($sql);
		if($consulta){
			return true;
		}else{
			return false;
		}
	}


	public function modificar($registro){

		//"UPDATE noticias SET idNot='7', tituloNot='nuevo titulo', textoNot='nuevo texto', fechaNot='132456987' WHERE idnot=7"

		$sql="UPDATE ".$this->tabla." SET ";

		$contador=0;

		foreach ($registro as $key => $value) {
				if($contador>0){
					$sql.=", ";
				}
				$sql.=$key."='".$value."'";
				$contador++;
		}

		$sql.=" WHERE ".$this->id."=".$registro[$this->id];

		$consulta=$this->conexion->query($sql);
		if($consulta){
			return true;
		}else{
			return false;
		}
	}

}



?>