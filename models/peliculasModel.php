<?php  

require('models/repositorioModel.php');
class Peliculas extends Repositorio{

	//conexion, datos, tabla, id ()

	public function __construct(){
		parent::__construct(); //constructor del padre
		$this->tabla='peliculas';
		$this->id='idPel';
		$this->model='Pelicula'; //nombre de la clase
	}

}
?>